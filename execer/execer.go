package execer

import (
	"fmt"
	"os"
	"os/exec"
	"sync"
)

type ExecConfig []string

func (e *ExecConfig) AsArray() *[]string {
	return (*[]string)(e)
}

func (e *ExecConfig) Name() string {
	return (*e.AsArray())[0]
}

type Exec struct {
	config    ExecConfig
	mu        sync.Mutex
	isRunning bool
}

func (c ExecConfig) Open() *Exec {
	return &Exec{
		config: c,
	}
}

func (e *Exec) Run() {
	if e.config == nil || len(*e.config.AsArray()) == 0 {
		return
	}

	e.mu.Lock()
	defer e.mu.Unlock()

	if e.isRunning {
		fmt.Printf("%s: already running\n", e.config.Name())
		return
	}

	e.isRunning = true
	fmt.Printf("%s: starting\n", e.config.Name())
	go func() {
		cmd := exec.Command(e.config.Name(), (*e.config.AsArray())[1:]...)
		cmd.Env = append(os.Environ(),
			fmt.Sprintf("BUTTOND_PID=%d", os.Getpid()),
		)
		cmd.Stdin = nil
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		err := cmd.Run()
		if err == nil {
			fmt.Printf("%s: finished successfully\n", e.config.Name())
		} else {
			fmt.Printf("%s: %v\n", e.config.Name(), err)
		}

		e.mu.Lock()
		defer e.mu.Unlock()
		e.isRunning = false
	}()
}
