module gitlab.com/pibutton/buttond

go 1.11

require (
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/warthog618/gpio v0.6.1
	gopkg.in/yaml.v2 v2.2.2
)
