package led

import (
	"sync"
	"time"

	"github.com/warthog618/gpio"
	"gitlab.com/pibutton/buttond/states"
)

type LedConfig struct {
	Active          states.ActiveLevel `yaml:"active"`
	OnExit          bool               `yaml:"on_exit"`
	FlashingOnStart bool               `yaml:"flashing_on_start"`
	TogglePeriod    int                `yaml:"toggle_period"`
	Pin             int                `yaml:"pin"`
}

type Led struct {
	config *LedConfig
	pin    *gpio.Pin

	// Mutex pretects access to isFlashing
	mu sync.Mutex

	// isFlashing indicates to the background thread when to terminate
	isFlashing bool
}

func (c *LedConfig) Open() *Led {
	l := &Led{
		config:     c,
		pin:        gpio.NewPin(c.Pin),
		isFlashing: false,
	}
	l.pin.Write(c.Active.Active())
	l.pin.Output()

	if c.FlashingOnStart {
		l.Flash()
	}

	return l
}

func (l *Led) Close() {
	if l.config.OnExit {
		l.On()
	} else {
		l.Off()
	}
}

func (l *Led) Flash() {
	l.mu.Lock()
	defer l.mu.Unlock()

	if l.isFlashing {
		return
	}

	l.isFlashing = true
	l.pin.Write(l.config.Active.Inactive())

	if l.config.TogglePeriod > 0 {
		time.AfterFunc(time.Duration(l.config.TogglePeriod)*time.Millisecond, l.flashTransition)
	}
}

func (l *Led) On() {
	l.mu.Lock()
	defer l.mu.Unlock()

	l.isFlashing = false
	l.pin.Write(l.config.Active.Active())
}

func (l *Led) Off() {
	l.mu.Lock()
	defer l.mu.Unlock()

	l.isFlashing = false
	l.pin.Write(l.config.Active.Inactive())
}

func (l *Led) flashTransition() {
	l.mu.Lock()
	defer l.mu.Unlock()

	if !l.isFlashing {
		return
	}

	l.pin.Toggle()
	time.AfterFunc(time.Duration(l.config.TogglePeriod)*time.Millisecond, l.flashTransition)
}
