package button

import (
	"fmt"
	"sync"
	"time"

	"github.com/warthog618/gpio"
	"gitlab.com/pibutton/buttond/execer"
	"gitlab.com/pibutton/buttond/states"
)

type Event string

const (
	bounceTimeout = 50 * time.Millisecond

	ShortPressEvent      Event = "short_press"
	LongPressEvent       Event = "long_press"
	RisingEdgeEvent      Event = "on_active"
	FallingEdgeEvent     Event = "on_inactive"
	StartupEvent         Event = "startup"
	StartupActiveEvent   Event = "startup_active"
	StartupInactiveEvent Event = "startup_inactive"
)

type ButtonConfig struct {
	Active      states.ActiveLevel          `yaml:"active"`
	Pull        states.PullName             `yaml:"pull"`
	Pin         int                         `yaml:"pin"`
	LongTimeout int                         `yaml:"long_timeout"`
	Commands    map[Event]execer.ExecConfig `yaml:"commands"`
}

type Button struct {
	config           *ButtonConfig
	pin              *gpio.Pin
	longPressTimeout time.Duration

	mu              sync.Mutex
	bounceTimer     *time.Timer
	buttonDownTimer *time.Timer
	lastState       gpio.Level

	commands map[Event]*execer.Exec
}

func (c *ButtonConfig) Open() (button *Button, err error) {
	b := &Button{
		config:           c,
		pin:              gpio.NewPin(c.Pin),
		longPressTimeout: time.Duration(c.LongTimeout) * time.Millisecond,
		commands:         make(map[Event]*execer.Exec),
	}
	b.pin.Input()

	for e, conf := range c.Commands {
		b.commands[Event(e)] = conf.Open()
		if e == LongPressEvent && c.LongTimeout <= 0 {
			return nil, fmt.Errorf("long_press event configured and long_timeout <= 0")
		}
	}

	if dpull, err := c.Pull.GpioPull(); err != nil {
		return nil, err
	} else {
		b.pin.SetPull(dpull)
	}

	b.lastState = b.pin.Read()
	b.fireEvent(StartupEvent)
	if b.config.Active.IsActive(b.lastState) {
		b.fireEvent(StartupActiveEvent)
	} else {
		b.fireEvent(StartupInactiveEvent)
	}

	if err := b.pin.Watch(gpio.EdgeBoth, b.onEdge); err != nil {
		return nil, fmt.Errorf("watching button gpio (%d): %v", c.Pin, err)
	}

	return b, nil
}

func (b *Button) onEdge(_ *gpio.Pin) {
	b.mu.Lock()
	defer b.mu.Unlock()

	// for every edge start a new timer and record which is the most recent timer started
	b.bounceTimer = time.NewTimer(bounceTimeout)
	go b.waitForBounceTimer(b.bounceTimer)
}

func (b *Button) waitForBounceTimer(bt *time.Timer) {
	<-bt.C

	b.mu.Lock()
	defer b.mu.Unlock()

	if b.bounceTimer != bt {
		// A bounce timer has been started more recently than this one
		return
	}

	r := b.pin.Read()
	if r != b.lastState {
		if b.config.Active.IsActive(r) {
			b.onActiveEdge()
		} else {
			b.onInactiveEdge()
		}
	}
	b.lastState = r
}

func (b *Button) onActiveEdge() {
	b.fireEvent(RisingEdgeEvent)
	b.buttonDownTimer = time.NewTimer(b.longPressTimeout)
	go b.onLongPressTimer(b.buttonDownTimer)
}

func (b *Button) onInactiveEdge() {
	b.fireEvent(FallingEdgeEvent)

	//don't fire event if the long press event has fired
	if b.buttonDownTimer != nil {
		b.fireEvent(ShortPressEvent)

		// ensure the long press event wont fire when the timer expires
		b.buttonDownTimer = nil
	}
}

func (b *Button) onLongPressTimer(t *time.Timer) {
	<-t.C

	b.mu.Lock()
	defer b.mu.Unlock()

	if b.buttonDownTimer != t {
		//no the most recent timer started
		return
	}

	// signal that the long press time has fired
	b.buttonDownTimer = nil

	b.fireEvent(LongPressEvent)
}

func (b *Button) fireEvent(e Event) {
	if cmd, exists := b.commands[e]; exists {
		cmd.Run()
	}
}
