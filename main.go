package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"syscall"

	"github.com/warthog618/gpio"
	"gitlab.com/pibutton/buttond/button"
	"gitlab.com/pibutton/buttond/led"
	"gopkg.in/yaml.v2"
)

type MainConfig struct {
	Led    led.LedConfig       `yaml:"led"`
	Button button.ButtonConfig `yaml:"button"`
}

var (
	mainConfig MainConfig
	ledInst    *led.Led
	buttonInst *button.Button
)

func run() error {
	if len(os.Args) != 2 {
		return fmt.Errorf("wrong number of argumentss\nusage: buttond config.yaml\n")
	}

	if data, err := ioutil.ReadFile(os.Args[1]); err != nil {
		return fmt.Errorf("reading config file: %v", err)
	} else {
		if err := yaml.Unmarshal(data, &mainConfig); err != nil {
			return fmt.Errorf("parsing config file: %v", err)
		}
	}

	if err := gpio.Open(); err != nil {
		return err
	}
	defer cleanup()

	ledInst = mainConfig.Led.Open()
	if b, err := mainConfig.Button.Open(); err != nil {
		return fmt.Errorf("configuring button: %v", err)
	} else {
		buttonInst = b
	}

	sigs := make(chan os.Signal)
	signal.Notify(sigs, syscall.SIGTERM, syscall.SIGINT, syscall.SIGUSR1, syscall.SIGUSR2)
	for {
		sig := <-sigs
		switch sig {
		case syscall.SIGTERM:
			return nil
		case syscall.SIGINT:
			return nil
		case syscall.SIGUSR1:
			ledInst.On()
		case syscall.SIGUSR2:
			ledInst.Flash()
		}
	}
}

func cleanup() {
	fmt.Println("exiting")
	if ledInst != nil {
		ledInst.Close()
	}
	gpio.Close()
}

func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
}
