package states

import (
	"fmt"

	"github.com/warthog618/gpio"
)

type PullName string

const (
	PullUp   = PullName("up")
	PullDown = PullName("down")
	PullNone = PullName("none")
)

func (p PullName) GpioPull() (gpio.Pull, error) {
	switch p {
	case PullUp:
		return gpio.PullUp, nil
	case PullDown:
		return gpio.PullDown, nil
	case PullNone, "":
		return gpio.PullNone, nil
	default:
		return gpio.PullNone, fmt.Errorf("unknown pull type: %q", p)
	}
}

type ActiveLevel bool

const (
	ActiveLow  = ActiveLevel(false)
	ActiveHigh = ActiveLevel(true)
)

func (a ActiveLevel) Active() gpio.Level {
	return gpio.Level(a)
}

func (a ActiveLevel) Inactive() gpio.Level {
	return gpio.Level(!a)
}

func (a ActiveLevel) IsActive(l gpio.Level) bool {
	return a.Active() == l
}

func (a ActiveLevel) IsInactive(l gpio.Level) bool {
	return a.Inactive() == l
}
